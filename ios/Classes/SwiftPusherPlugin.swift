import Flutter
import UIKit
import PusherSwift

public class SwiftPusherPlugin: NSObject, FlutterPlugin {
    
    static var channel:FlutterMethodChannel?
    var pusher:Pusher?
    var channels:[PusherChannel] = []
    var channelEvents:[ChannelEvent] = []
    
    override init() {
    }
    
    public static func register(with registrar: FlutterPluginRegistrar) {
        channel = FlutterMethodChannel(name: "pusher.raptors", binaryMessenger: registrar.messenger())
        let instance = SwiftPusherPlugin()
        registrar.addMethodCallDelegate(instance, channel: channel!)
    }
    
    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        //result("iOS " + UIDevice.current.systemVersion)
        let param = call.arguments as? [String:Any]
        print("-> handle")
        print(call.method)
        print(param!)
        
        switch call.method {
        case "initPusher":
            
            initPusher(appUrl: param?["appUrl"] as! String,
                       appToken: param?["appToken"] as! String,
                       appCluster: param?["appCluster"] as! String,
                       appKey: param?["appKey"] as! String)
            result(true)
            break
        case "disconnectPusher":
            disconnectPusher()
            result(true)
            break
        case "subscribeOnChannel":
            subscribeOnChannel(appChannel: param?["appChannel"] as! String,
                               privateEvent: param?["privateEvent"] as! Int)
            break
        case "unSubscribeOnChannel":
            unSubscribeOnChannel(appChannel: param?["appChannel"] as! String)
            break
        case "bindOnChannel":
            bindOnChannel(appChannel: param?["appChannel"] as! String,
                          appEvent: param?["appEvent"] as! String)
            break
        case "unBindOnChannel":
            unBindOnChannel(appChannel: param?["appChannel"] as! String,
                            appEvent: param?["appEvent"] as! String)
            break
        default:
            result(FlutterMethodNotImplemented)
            break
        }
        
    }
    
    public func initPusher(appUrl:String, appToken:String, appCluster:String, appKey:String) {
        print("initPusher")
        let options = PusherClientOptions(
            authMethod: AuthMethod.authRequestBuilder(authRequestBuilder: AuthRequestBuilder(url: appUrl, token: appToken)),
            autoReconnect: true,
            host: .cluster(appCluster)
        )
        
        self.pusher = Pusher(key: appKey, options: options)
        //self.pusher?.delegate = self
        self.pusher?.connection.delegate = self
        self.pusher?.connect()
    }
    
    public func disconnectPusher(){
        print("disconnectPusher")
        self.pusher?.disconnect()
    }
    
    public func subscribeOnChannel(appChannel:String, privateEvent:Int){
        print("subscribeOnChannel")
        if let chan = self.pusher?.subscribe(appChannel) {
            channels.append(chan)
        }
    }
    
    public func unSubscribeOnChannel(appChannel:String){
        print("unSubscribeOnChannel")
        self.pusher?.unsubscribe(appChannel)
        if let index = self.channels.firstIndex(where: {$0.name == appChannel}){
            channels.remove(at: index)
        }
    }
    
    public func bindOnChannel(appChannel:String, appEvent:String){
        print("bindOnChannel")
        if let chan = self.channels.first(where: {$0.name == appChannel}) {
            let id =  chan.bind(eventName: appEvent, eventCallback: { (result) in
                print(result.data!)
                self.returnData(data: result.data!, event: appEvent)
//                if let jsonData = try? JSONSerialization.data(withJSONObject: result.data!, options:[]) {
//                        self.returnData(data: String(bytes: jsonData, encoding: String.Encoding.utf8)!, event: appEvent)
//                } else {
//                    self.returnData(data: "", event: appEvent)
//                }
               
            })
            channelEvents.append(ChannelEvent(id: id, channel: appChannel, event: appEvent))
        }
    }
    
    public func unBindOnChannel(appChannel:String, appEvent:String){
        print("unBindOnChannel")
        if let chan = self.channels.first(where: {$0.name == appChannel}) {
            let events = channelEvents.filter({$0.channel == appChannel && $0.event == appEvent})
            events.forEach { (e) in
                chan.unbind(eventName: e.event, callbackId: e.id)
            }
        }
    }
    
    public func returnData(data:String, event:String){
        print("returnData")
        SwiftPusherPlugin.channel?.invokeMethod(event, arguments: data)
    }
}

extension SwiftPusherPlugin: PusherDelegate {
    
    public func changedConnectionState(from old: ConnectionState, to new: ConnectionState) {
        print("changedConnectionState")
        print(new.stringValue())
    }
    
    public func debugLog(message: String) {
        print("debugLog:")
        print(message)
    }
    
    public func subscribedToChannel(name: String) {
        print("subscribedToChannel:")
        print(name)
    }
    
    public func failedToSubscribeToChannel(name: String, response: URLResponse?, data: String?, error: NSError?) {
        print("failedToSubscribeToChannel:")
        print(name)
    }
}

struct ChannelEvent {
    var id:String
    var channel:String
    var event:String
}

class AuthRequestBuilder: AuthRequestBuilderProtocol {
    
    var _url:String = ""
    var _token:String = ""
    
    init(url:String, token:String) {
        _url = url
        _token = token
    }
    
    func requestFor(socketID: String, channelName: String) -> URLRequest? {
        var request = URLRequest(url: URL(string: _url)!)
        request.httpMethod = "POST"
        request.httpBody = "socket_id=\(socketID)&channel_name=\(channelName)".data(using: String.Encoding.utf8)
        request.addValue("Bearer \(_token)", forHTTPHeaderField: "Authorization")
        return request
    }
}
