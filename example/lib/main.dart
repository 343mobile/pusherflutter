import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:pusher/pusher.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Pusher pusher;
  String _result = '';
  StreamController<PusherResult> streamController;

  void onReceiveDataFromPusher(String result) {
    setState(() {
      _result = result;
    });
  }

  @override
  void dispose() {
    streamController.close();
    //pusher.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    pusher = new Pusher();
    pusher.init(
        'https://service.appmove.me/api/pusher/auth',
        'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjFhYTJjODIwZGRkZjRhMTUwY2IxMGZlYzRjNzZjM2VlNTUwNzIxODkzZTU2YWU2ODE2OTRlM2YwNDA1NTI5OWU3YjNkMTE0NjA3ZTYxZWRiIn0.eyJhdWQiOiIxIiwianRpIjoiMWFhMmM4MjBkZGRmNGExNTBjYjEwZmVjNGM3NmMzZWU1NTA3MjE4OTNlNTZhZTY4MTY5NGUzZjA0MDU1Mjk5ZTdiM2QxMTQ2MDdlNjFlZGIiLCJpYXQiOjE1NTk1OTI0NTcsIm5iZiI6MTU1OTU5MjQ1NywiZXhwIjoxNTkxMjE0ODU3LCJzdWIiOiJlYjNkMjQ4OC00YzQ5LTQ4ZWMtYjZkYi1hMTk1ZWQyZjQ2ZWIiLCJzY29wZXMiOlsibW9iaWxlIl19.Z9HWCRcwW3jYVxZDA38SX-nG6MFlS01jpgCV-EEG3g_Ao9XFPpXqObUlWo2X8t3wih5pw7jj5-5PGyu9bMCKNG4FA7hTgXUW9siJjXKGaOVC5gb4UBuT9S5y-Mzb5uDzoWq2_ZwE08UJQ7ixO1bOrqeBUNs6vtvQcTjlnmlkScmUum1t2X8C-ZO5WSRMDDLWvjKBTqUK6ag3HU-HGqMIMQ_R2XItLLtltuB7olj0ejElyM39I5AcKBzqWIAnRd5r91a5duDDkp1T-N5vQyRcCHCAzC-oAy-b38bMaPVfXBD0rungCPXombDwKTh-gJEK83ACdR8YTpt-WjFK3EWyO4dx1pCqAKKvtjlw9pVN4gxmMeMuxiOszRzOEzE_aP24WReWWErIO5ZhxyXMdYLjkW0C7z4wuHG8aHP4Ls1W7U7QMEriGAGnF1PZWeNwCbmjUV4fVuq79CpmM9hSQ_UM_QZ4AbcC3LHVlgIQ9D7eWhBKNtHK3cZ-K9wTPhK3otqbMPv9HJYLS0TPgMlbzMCvhMH8D1qRvECUjZtCgL_D5ZzmEwvwF-p_OtdPUYjk3pw4v81lWKwL0nugZ0iwmswSlM_Db4dnXag8Nfsr2FOIENW4tdgbTiW4MjPeVoyH212DG_DY-WGUsnM5fVEMDUJv7gp4T-gvHbO28r6vlMwe4Fc',
        'us3',
        'ad4190c441b012ee7842');

    pusher.callback = resultPusher;
  }

  void resultPusher(PusherResult result) {
    print(result);
    setState(() {
      _result = jsonEncode(result.data);
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: FlatButton(
                        color: Colors.blue,
                        onPressed: () {
                          pusher.subscribeToAChannel('my-channel', false);
                        },
                        child: new Text('Channel', style: TextStyle(color: Colors.white))),
                  ),
                  SizedBox(width: 20.0),
                  Expanded(
                    child: FlatButton(
                        color: Colors.blue,
                        onPressed: () {
                          pusher.bindOnAChannel('my-channel', 'my-event', false);
                        },
                        child: new Text('Event', style: TextStyle(color: Colors.white))),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: FlatButton(
                        color: Colors.blue,
                        onPressed: () {
                          pusher.unSubscribeToAChannel('my-channel');
                        },
                        child: new Text('unSubscribe', style: TextStyle(color: Colors.white))),
                  ),
                  SizedBox(width: 20.0),
                  Expanded(
                    child: FlatButton(
                        color: Colors.blue,
                        onPressed: () {
                          pusher.unBindOnAChannel('my-channel', 'my-event');
                        },
                        child: new Text('unEvent', style: TextStyle(color: Colors.white))),
                  )
                ],
              ),
              Center(child: Text(_result)),
              Center(child: Text(new DateTime.now().toIso8601String()))
            ],
          ),
        ),
      ),
    );
  }
}
