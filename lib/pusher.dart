import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

class Pusher {
  static const MethodChannel _channel = const MethodChannel('pusher.raptors');
  static final Pusher _pusher = new Pusher._internal();
  Function callback;

  factory Pusher() {
    return _pusher;
  }

  Pusher._internal();

  init(String url, String token, String cluster, String key) async {
    _channel.setMethodCallHandler(handleMethod);
    var param = {'appUrl': url, 'appToken': token, 'appCluster': cluster, 'appKey': key};

    try {
      final bool result = await _channel.invokeMethod('initPusher', param);
      print('init: $result');
    } on PlatformException catch (e) {
      print(e);
    }
  }

  subscribeToAChannel(String channel, bool privateEvent) async {
    var param = {'appChannel': channel, 'privateEvent': privateEvent};

    try {
      final bool result = await _channel.invokeMethod('subscribeOnChannel', param);
      print('subscribeToAChannel: $result');
    } on PlatformException catch (e) {
      print(e);
    }
  }

  unSubscribeToAChannel(String channel) async {
    var param = {'appChannel': channel};

    try {
      final bool result = await _channel.invokeMethod('unSubscribeOnChannel', param);
      print('unSubscribeToAChannel: $result');
    } on PlatformException catch (e) {
      print(e);
    }
  }

  bindOnAChannel(String channel, String event, bool privateEvent) async {
    var param = {'appChannel': channel, 'appEvent': event, 'privateEvent': privateEvent};

    try {
      final bool result = await _channel.invokeMethod('bindOnChannel', param);
      print('bindOnAChannel: $result');
    } on PlatformException catch (e) {
      print(e);
    }
  }

  unBindOnAChannel(String channel, String event) async {
    var param = {'appChannel': channel, 'appEvent': event};

    try {
      final bool result = await _channel.invokeMethod('unBindOnChannel', param);
      print('unBindOnAChannel: $result');
    } on PlatformException catch (e) {
      print(e);
    }
  }

  disconnect() async {
    try {
      final bool result = await _channel.invokeMethod('disconnectPusher');
      print('disconnect: $result');
    } on PlatformException catch (e) {
      print(e);
    }
  }

  Future<dynamic> handleMethod(MethodCall call) async {
    print("***********************************New Pusher Event*************************************");
    if (callback != null) callback(new PusherResult(event: call.method, data: json.decode(call.arguments)));
  }
}

class PusherResult {
  String event;
  Map data;

  PusherResult({@required this.event, @required this.data});
}
