package br.com.raptors.pusher;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.ChannelEventListener;
import com.pusher.client.channel.PrivateChannelEventListener;
import com.pusher.client.channel.PusherEvent;
import com.pusher.client.channel.SubscriptionEventListener;
import com.pusher.client.util.HttpAuthorizer;
import com.pusher.client.connection.ConnectionEventListener;
import com.pusher.client.connection.ConnectionStateChange;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry.Registrar;

public class PusherPlugin implements MethodCallHandler {
    private static Pusher pusher;
    private static final String CHANNEL = "pusher.raptors";
    private static MethodChannel channel;
    private static List<Channel> pusherChannels = new ArrayList<>();

    public static void registerWith(Registrar registrar) {
        channel = new MethodChannel(registrar.messenger(), CHANNEL);
        channel.setMethodCallHandler(new PusherPlugin());
    }

    @Override
    public void onMethodCall(MethodCall call, Result result) {

        if (call.arguments != null)
            Log.d("Pusher Plugin", call.arguments.toString());
        else
            Log.d("Pusher Plugin", "EMPTY");

        switch (call.method) {
            case "initPusher":
                initPusher(
                        call.argument("appUrl"),
                        call.argument("appToken"),
                        call.argument("appCluster"),
                        call.argument("appKey")
                );
                result.success(true);
                break;
            case "disconnectPusher":
                disconnectPusher();
                result.success(true);
                break;
            case "subscribeOnChannel":
                subscribeOnChannel(
                        call.argument("appChannel"),
                        call.argument("privateEvent")
                );
                result.success(true);
                break;
            case "unSubscribeOnChannel":
                unSubscribeOnChannel(call.argument("appChannel"));
                result.success(true);
                break;
            case "bindOnChannel":
                bindOnChannel(
                        call.argument("appChannel"),
                        call.argument("appEvent"),
                        call.argument("privateEvent")
                );
                result.success(true);
                break;
            case "unBindOnChannel":
                unBindOnChannel(
                        call.argument("appChannel"),
                        call.argument("appEvent")
                );
                result.success(true);
                break;
            default:
                result.notImplemented();
                break;
        }
    }

    private void initPusher(String appUrl, String appToken, String appCluster, String appKey) {
        PusherOptions options = new PusherOptions();
        options.setCluster(appCluster);

        HttpAuthorizer authorizer = new HttpAuthorizer(appUrl);
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");
        headers.put("Authorization", "Bearer " + appToken);
        authorizer.setHeaders(headers);

        options.setAuthorizer(authorizer);

        pusher = new Pusher(appKey, options);
        pusher.connect(new ConnectionEventListener() {
            @Override
            public void onConnectionStateChange(ConnectionStateChange connectionStateChange) {
                Log.d("Pusher Plugin", connectionStateChange.getCurrentState().toString());
            }

            @Override
            public void onError(String s, String s1, Exception e) {
                Log.d("Pusher Plugin", "Connection Error");
                if (s != null) Log.d("Pusher Plugin", s);
                if (s1 != null) Log.d("Pusher Plugin", s1);
                if (e != null) Log.d("Pusher Plugin", e.getMessage());
            }
        });
    }

    private void disconnectPusher() {
        if (pusher != null) {
            Log.d("Pusher Plugin", "DISCONNECTING");
            pusher.disconnect();
            pusherChannels.clear();
        }
    }

    private void subscribeOnChannel(String appChannel, Boolean privateEvent) {

        if (pusher == null) return;
        for (int i = 0; i < pusherChannels.size(); i++) {
            if (pusherChannels.get(i).getName().equals(appChannel)) return;
        }

        if (privateEvent) {
            Channel channel = pusher.subscribePrivate(appChannel);
            pusherChannels.add(channel);
        } else {
            Channel channel = pusher.subscribe(appChannel);
            pusherChannels.add(channel);
        }
    }

    private void unSubscribeOnChannel(String appChannel) {
        for (int i = 0; i < pusherChannels.size(); i++) {
            if (pusherChannels.get(i).getName().equals(appChannel))
                pusherChannels.remove(pusherChannels.get(i));
        }
        pusher.unsubscribe(appChannel);
    }

    private void bindOnChannel(String appChannel, String appEvent, Boolean privateEvent) {
        if (pusher == null) return;
        if (pusherChannels.size() == 0) return;

        Channel ch = null;

        for (int i = 0; i < pusherChannels.size(); i++) {
            if (pusherChannels.get(i).getName().equals(appChannel)) ch = pusherChannels.get(i);
        }

        if (ch == null) return;

        if (privateEvent) {
            ch.bind(appEvent, new PrivateChannelEventListener() {
                @Override
                public void onEvent(PusherEvent event) {
                    Log.d("Pusher Plugin", event.getData());
                    returnData(event.getData(), event.getEventName());
                }

                @Override
                public void onAuthenticationFailure(String s, Exception e) {
                    Log.d("Pusher Plugin:", s);
                    Log.d("Pusher Plugin", "Auth Error");
                    if (e != null) Log.d("Pusher Plugin", e.getMessage());
                }

                @Override
                public void onSubscriptionSucceeded(String s) {
                    Log.d("Pusher Plugin", s);
                }

            });
        } else {
            ch.bind(appEvent, event -> {
                Log.d("Pusher Plugin", event.getData());
                returnData(event.getData(), event.getEventName());
            });
        }
    }

    private void unBindOnChannel(String appChannel, String appEvent) {
        if (pusher == null) return;
        if (pusherChannels.size() == 0) return;

        Channel ch = null;

        for (int i = 0; i < pusherChannels.size(); i++) {
            if (pusherChannels.get(i).getName().equals(appChannel)) ch = pusherChannels.get(i);
        }

        if (ch == null) return;

        ch.unbind(appEvent, null);
    }

    private void returnData(String data, String event) {
        new Handler(Looper.getMainLooper()).post(() -> channel.invokeMethod(event, data));
    }
}
